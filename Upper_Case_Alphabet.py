# count upper case in text and find the largest and the least
O_File = open("inputTEXT.txt","r")
My_File = O_File.read()
Sequnce = {}
Upper_Case = list(map(chr, range(65, 91)))
for characters in My_File:
    if characters in Sequnce:
            if characters in Upper_Case:
                Sequnce[characters] = Sequnce[characters] + 1
    elif characters in Upper_Case:
        Sequnce[characters] = 1
for letters in Sequnce:
    print(f"{letters} : {Sequnce[letters]} \n")

L_min = min(Sequnce.keys(), key=(lambda k: Sequnce[k]))
print(f"the letter with the least number is : {L_min}")
L_max= max(Sequnce.keys(), key=(lambda k: Sequnce[k]))
print(f"the letter with the largest number is {L_max}")

# show the result in a new text file
New_F = open("Uppercase_result.txt", "a")
for letters in Sequnce:
    New_F.write(f"{letters} : {Sequnce[letters]} \n")
New_F.close()

