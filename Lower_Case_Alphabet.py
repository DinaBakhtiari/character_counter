# count lower case in text and find the least and the largest 
O_File = open("inputTEXT.txt","r")
My_File = O_File.read()
Sequnce = {}
Lower_Case = list(map(chr, range(97, 123)))
for characters in My_File:
    if characters in Sequnce:
            if characters in Lower_Case:
                Sequnce[characters] = Sequnce[characters] + 1
    elif characters in Lower_Case:
        Sequnce[characters] = 1
for letters in Sequnce:
    print(f"{letters} : {Sequnce[letters]} \n")

L_min = min(Sequnce.keys(), key=(lambda k: Sequnce[k]))
print(f"the letter with the least number is : {L_min}")
L_max= max(Sequnce.keys(), key=(lambda k: Sequnce[k]))
print(f"the letter with the largest number is {L_max}")
# show the result in text file :
New_F = open("Lowercase_result.txt", "a")
for letters in Sequnce:
    New_F.write(f"{letters} : {Sequnce[letters]} \n")
New_F.close()
